export const INVALID_EMAIL_OR_PASSWORD = "Invalid e-mail or password";
export const MISSING_EMAIL_AND_PASSWORD = "Missing email and password fields";
export const MISSING_EMAIL_OR_PASSWORD = "Missing email or password fields";
export const USER_DOES_NOT_EXIST = "User does not exist";