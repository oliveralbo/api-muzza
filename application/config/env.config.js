module.exports = {
    "port": process.env.PORT || 5000,
    "jwt_secret": process.env.JWT_SECRET || "zOsgBpJYRRbC72uoVYpsoQPzFgDIh0YX",
    "jwt_expiration_in_seconds": process.env.JWT_EXPIRATION || 36000,
    "permissionLevels": {
        "NORMAL_USER": 1,
        "PAID_USER": 4,
        "ADMIN": 2048
    }
};