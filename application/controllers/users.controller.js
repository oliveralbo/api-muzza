import HttpStatus from 'http-status-codes';
import _ from 'lodash';
import Bcrypt from 'bcrypt';
import UserModel from '../models/users.model';
import { USER_DOES_NOT_EXIST } from '../config/const.config';

exports.insert = (req, res) => {
  const {
    body: {
      name, surname, email, password,
    },
  } = req;

  const user = {
    name,
    surname,
    email,
    password: password && Bcrypt.hashSync(password, 10),
  };

  UserModel.create(user)
    .then((result) => {
      res.status(HttpStatus.OK).send(result);
    }, (err) => {
      res.status(HttpStatus.BAD_REQUEST).send({ errors: err.errors });
    });
};


exports.getAll = (req, res) => {
  UserModel.getAll()
    .then((result) => {
      res.status(HttpStatus.OK).send(result);
    }, (err) => {
      res.status(HttpStatus.BAD_REQUEST).send({ errors: err });
    });
};


exports.isUserMatch = (req, res, next) => {
  const { id } = req.params;
  // eslint-disable-next-line consistent-return
  UserModel.findById(id).then((user, err) => {
    if (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ errors: err });
    }
    if (_.isEmpty(user)) {
      return res.status(HttpStatus.BAD_REQUEST).send({ errors: [USER_DOES_NOT_EXIST] });
    }
    next();
  });
};


exports.delete = (req, res) => {
  const { id } = req.params;
  UserModel.delete(id)
    .then((result) => {
      res.status(HttpStatus.OK).send(result);
    }, (error) => {
      res.status(HttpStatus.BAD_REQUEST).send(error);
    });
};
