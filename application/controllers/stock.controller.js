import HttpStatus from 'http-status-codes';
import StockModel from '../models/stock.model';

exports.getAll = (req, res) => {
  StockModel.getAll()
    .then((result) => {
      res.status(HttpStatus.OK).send(result);
    }, (err) => {
      res.status(HttpStatus.BAD_REQUEST).send({ errors: err });
    });
};
