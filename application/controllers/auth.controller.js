import _ from 'lodash';
import Bcrypt from 'bcrypt';
import Jwt from 'jsonwebtoken';
import HttpStatus from 'http-status-codes';
import config from '../config/env.config';
import { INVALID_EMAIL_OR_PASSWORD, MISSING_EMAIL_AND_PASSWORD, MISSING_EMAIL_OR_PASSWORD } from '../config/const.config';
import UserModel from '../models/users.model';

exports.isPasswordAndUserMatch = (req, res, next) => {
  const { body: { email, password } } = req;

  UserModel.findByUsername(email).then((user, err) => {
    if (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ errors: err });
    }
    if (Bcrypt.compareSync(password, user ? user.password : '')) {
      req.body = user;
      return next();
    }
    return res.status(HttpStatus.BAD_REQUEST).send({ errors: [INVALID_EMAIL_OR_PASSWORD] });
  });
};


exports.hasAuthValidFields = (req, res, next) => {
  const { body, body: { email, password } } = req;

  if (!_.isEmpty(body)) {
    if (!email || !password) {
      return res.status(HttpStatus.BAD_REQUEST).send({ errors: MISSING_EMAIL_OR_PASSWORD });
    }
    return next();
  }
  return res.status(HttpStatus.BAD_REQUEST).send({ errors: MISSING_EMAIL_AND_PASSWORD });
};


exports.login = (req, res) => {
  const { body } = req;
  try {
    const token = Jwt.sign({ user: body },
      config.jwt_secret, { expiresIn: config.jwt_expiration_in_seconds });

    res.status(HttpStatus.OK).send({
      user: body,
      token,
    });
  } catch (err) {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ errors: err });
  }
};
