import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import bearerToken from 'express-bearer-token';
import config from "./config/env.config";
import Logger from "./config/logger.config";
import UsersRouter from './routes/users.route';
import LoginRouter from './routes/auth.route';

const app = express();

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(bearerToken())
app.use(Logger)


UsersRouter.routesConfig(app);
LoginRouter.routesConfig(app);

app.listen(config.port, function () {
  console.log("\x1b[34m", 'Application listening at port '+ config.port , "\x1b[0m");
});