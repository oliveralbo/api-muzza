import Jwt from 'jsonwebtoken';
import config from '../config/env.config';

exports.validJWTNeeded = (req, res, next) => {

    let { token } = req;
    if(token){
        Jwt.verify(token, config.jwt_secret, (err, decode) => {
            if(err){
                return res.status(401).send({
                    err
                });
            }
            req.jwt = decode.user;
            next();
        })
    }else{
        return res.status(401).send();
    }
};

exports.onlyAdminCanDoThisAction = (req, res, next) => {
    let  { jwt : { permissionLevel }} = req;
    if (permissionLevel == parseInt(config.permissionLevels.ADMIN)) {
        return next();
    } 
    return res.status(403).send();
    
};