import uniqueValidator from 'mongoose-unique-validator';
import isEmail from 'validator/lib/isEmail';
import { mongoose } from '../services/mongoose.service';
import config from '../config/env.config';

const { Schema } = mongoose;

const userSchema = Schema({
  name: {
    type: String,
    required: [true, 'El nombre es requerido'],
  },
  surname: {
    type: String,
    required: [true, 'El apellido es requerido'],
  },
  email: {
    type: String,
    unique: true,
    validate: [isEmail, 'El email es invalido'],
    required: [true, 'El email es requerido'],
  },
  permissionLevel: {
    type: Number,
    default: config.permissionLevels.NORMAL_USER,
  },
  password: {
    type: String,
    required: [true, 'La contraseña es requerida'],
  },
  active: {
    type: Boolean,
    default: false,
  },
});

userSchema.methods.toJSON = function toObject() {
  const object = this.toObject();
  delete object.password;
  delete object.active;
  return object;
};

const User = mongoose.model('Users', userSchema);

userSchema.plugin(uniqueValidator, { message: 'El {PATH} debe de ser único' });


exports.create = (user) => new User(user).save();
exports.findByUsername = (email) => User.findOne({ email });
exports.findById = (id) => User.findOne({ _id: id });
exports.getAll = () => User.find({ active: false });
exports.delete = (id) => User.findOneAndUpdate({ _id: id }, { active: true }, { new: true }, (user) => user);
