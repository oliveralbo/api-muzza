import { mongoose } from '../services/mongoose.service';

const { Schema } = mongoose;

const stockSchema = Schema({
  provider: {
    type: String,
    required: [true],
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'StockCategory',
  },
  surname: {
    type: String,
    required: [true],
  },
});


// eslint-disable-next-line no-unused-vars
const Stock = mongoose.model('Stock', stockSchema);
