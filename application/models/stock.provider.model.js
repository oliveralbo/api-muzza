/* eslint-disable no-unused-vars */
import { mongoose } from '../services/mongoose.service';

const { Schema } = mongoose;

const providerSchema = Schema({
  name: {
    type: String,
    required: [true],
  },
  description: {
    type: String,
    required: [true],
  },
});


const Provider = mongoose.model('Provider', providerSchema);
