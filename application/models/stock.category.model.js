import { mongoose } from '../services/mongoose.service';

const { Schema } = mongoose;

const stockCategoySchema = Schema({
  name: {
    type: String,
    required: [true, 'El nombre es requerido'],
  },
  description: {
    type: String,
    required: [true, 'El apellido es requerido'],
  },
});


// eslint-disable-next-line no-unused-vars
const StockCategory = mongoose.model('StockCategory', stockCategoySchema);
