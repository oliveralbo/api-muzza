import UsersController from '../controllers/users.controller';
import AuthMiddleware from '../middlewares/auth.middleware';

exports.routesConfig = (app) => {
  app.get('/users/', [
    AuthMiddleware.validJWTNeeded,
    AuthMiddleware.onlyAdminCanDoThisAction,
    UsersController.getAll,
  ]);

  app.delete('/users/:id', [
    AuthMiddleware.validJWTNeeded,
    AuthMiddleware.onlyAdminCanDoThisAction,
    UsersController.isUserMatch,
    UsersController.delete,
  ]);

  app.post('/users/', [
    UsersController.insert,
  ]);
};
