import AuthController from '../controllers/auth.controller';

exports.routesConfig = (app) => {
  app.post('/login', [
    AuthController.hasAuthValidFields,
    AuthController.isPasswordAndUserMatch,
    AuthController.login,
  ]);
};
