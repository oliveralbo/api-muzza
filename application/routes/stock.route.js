import StockController from '../controllers/stock.controller';
import AuthMiddleware from '../middlewares/auth.middleware';

exports.routesConfig = (app) => {
  app.get('/stock/', [
    AuthMiddleware.validJWTNeeded,
    AuthMiddleware.onlyAdminCanDoThisAction,
    StockController.getAll,
  ]);
  /*
    app.delete('/stock/:id', [
        AuthMiddleware.validJWTNeeded,
        AuthMiddleware.onlyAdminCanDoThisAction,
        UsersController.isUserMatch,
        UsersController.delete
    ]); */

  /*  app.post('/stock/', [
        UsersController.insert
    ]); */
};
